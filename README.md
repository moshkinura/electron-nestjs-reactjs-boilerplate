Electron+Nest.js+React.js Boilerplate

============

#### © by Yury Moshkin 2024

Запуск:

`yarn install` - Установить зависимости

`yarn start` - Запустить Electron локально

`yarn build` - Создать билд (особого применения нет)

`yarn make` - Создать релиз (установщик)

# Контакты для связи

Telegram: [@bikeauto](https://t.me/bikeauto)
