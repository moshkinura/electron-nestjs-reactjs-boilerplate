import { useEffect, useState } from 'react';
import axios from 'axios';

export const Component = () => {
  const [data, setData]: any = useState();

  useEffect(() => {
    const getData = async () => {
      const getText = await axios.get('http://localhost:3000');
      setData(getText.data);
    };
    getData();
  }, []);

  console.log(data);

  return <>{data?.text}</>;
};
