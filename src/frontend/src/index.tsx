import { createRoot } from 'react-dom/client';

import { Component } from './Component';

// Render your React component instead
const root = createRoot(document.getElementById('root') as Element);
root.render(
  <>
    <h1>Hello, world</h1> and <Component />
  </>,
);
