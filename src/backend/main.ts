import { app, BrowserWindow } from 'electron';
import { NestFactory } from '@nestjs/core';

import url from 'url';
import path from 'path';

import { AppModule } from './src/app.module';

let mainWindow: BrowserWindow | null;

const createWindow = async () => {
  try {
    mainWindow = new BrowserWindow({
      width: 800,
      height: 600,
      webPreferences: {
        nodeIntegration: true,
      },
    });

    await mainWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, '../frontend/index.html'),
        protocol: 'file:',
        slashes: true,
      }),
    );

    mainWindow.webContents.openDevTools();

    mainWindow.on('closed', () => {
      mainWindow = null;
    });
  } catch (error) {
    console.error(error);
  }
};

const bootstrapNestJs = async () => {
  const app = await NestFactory.create(AppModule);

  await app.listen(3000);
};

app.on('ready', async () => {
  await bootstrapNestJs();
  createWindow();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
